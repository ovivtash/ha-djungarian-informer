#### TeamV Informer -=- Djungarian edition

Simple Nodemcu-based informer with WS2812 LED, buzzer and button.

![Schematics](djungarian_bb.png)