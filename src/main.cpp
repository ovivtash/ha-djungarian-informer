#include <Arduino.h>
#include <Homie.h>

#include <Adafruit_NeoPixel.h>
#include "OneButton.h"

#define RTTTL_NONBLOCKING_INFO
#include <NonBlockingRtttl.h>


const int PIN_BUTTON = D5;
const int PIN_PIXEL = D6;
const int PIN_BUZZER = D7;
const int PIXEL_COUNT = 3;

const char* NORMAL_MELODY = "boot:d=10,o=6,b=180,c,e,g";
const char* CONFIG_MELODY = "smb_under:d=4,o=6,b=100:32c,32p,32c7,32p,32a5,32p,32a,32p,32a#5,32p,32a#";

OneButton button(PIN_BUTTON, false, false);
HomieNode buttonNode("button", "button");

const int MELODY_BUFFER_SIZE = 4096;
char melodyBuffer[MELODY_BUFFER_SIZE];
bool lastIsPlaying = false;
HomieNode melodyNode("melody", "rtttl");

Adafruit_NeoPixel pixels(PIXEL_COUNT, PIN_PIXEL, NEO_GRB + NEO_KHZ800);
// Argument 1 = Number of pixels in NeoPixel strip
// Argument 2 = Arduino pin number (most are valid)
// Argument 3 = Pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
//   NEO_RGBW    Pixels are wired for RGBW bitstream (NeoPixel RGBW products)
HomieNode pixelNode("led", "RGB LED");

void setLedColor(uint8_t r, uint8_t g, uint8_t b){
  for(int i=0; i<PIXEL_COUNT;i++){
    pixels.setPixelColor(i, pixels.Color(r, g, b));
  }
  pixels.show();
}

void setLedOff(){
  pixels.clear();
  pixels.show();
}


void onHomieEvent(const HomieEvent& event) {
  switch(event.type) {
    case HomieEventType::CONFIGURATION_MODE:
      // Do whatever you want when configuration mode is started
      setLedColor(212, 125, 210);
      rtttl::begin(PIN_BUZZER, CONFIG_MELODY);
      break;
    case HomieEventType::NORMAL_MODE:
      // Do whatever you want when normal mode is started
      rtttl::begin(PIN_BUZZER, NORMAL_MELODY);
      break;
  }
}

bool ledColorInputHandler(const HomieRange& range, const String& value){
  int commaIndex = value.indexOf(',');
  int secondCommaIndex = value.indexOf(',', commaIndex + 1); //  Search for the next comma just after the first
  if(commaIndex != -1 && secondCommaIndex != -1 && secondCommaIndex > commaIndex){
    String redValue = value.substring(0, commaIndex);
    String greenValue = value.substring(commaIndex + 1, secondCommaIndex);
    String blueValue = value.substring(secondCommaIndex + 1); 
    int r = redValue.toInt();
    int g = greenValue.toInt();
    int b = blueValue.toInt();
    setLedColor(r, g, b);
    pixelNode.setProperty("color").send(value);
    return true;
  }
  return false;
}

bool ledBrightnessInputHandler(const HomieRange& range, const String& value){
  int br = value.toInt();
  //Homie.getLogger() << "Brightness requested: " << value << endl;
  if(br >=0 && br <= 255){
    pixels.setBrightness(br);
    pixels.show();
    pixelNode.setProperty("brightness").send(value);
    return true;
  }
  return false;
}

bool melodyInputHandler(const HomieRange& range, const String& value){
  value.toCharArray(melodyBuffer, MELODY_BUFFER_SIZE);
  rtttl::stop();
  rtttl::begin(PIN_BUZZER, melodyBuffer);
  return true;
}

void onSingleClick() {
     buttonNode.setProperty("click").send("single");
}

void onDoubleClick() {
     buttonNode.setProperty("click").send("double");
}

void setup() {
  Serial.begin(115200);
  Serial << endl << endl;
  pinMode(PIN_BUTTON, INPUT);
  digitalWrite(PIN_BUTTON, HIGH);
  pinMode(PIN_BUZZER, OUTPUT);

  pixels.begin();

  Homie_setBrand("TeamV");
  Homie_setFirmware("djungarian-informer", "1.0.0");

  buttonNode.advertise("click");
  pixelNode.advertise("color").settable(ledColorInputHandler);
  pixelNode.advertise("brightness").settable(ledBrightnessInputHandler);
  melodyNode.advertise("rtttl").settable(melodyInputHandler);
  melodyNode.advertise("playing");

  Homie.onEvent(onHomieEvent);
  Homie.setup();

  button.attachClick(onSingleClick);
  button.attachDoubleClick(onDoubleClick);

  setLedOff();
}

void loop() {
  if(rtttl::isPlaying()){
    if(!lastIsPlaying){
      melodyNode.setProperty("playing").send("true");
      lastIsPlaying = true;
    }
    rtttl::play();
  }
  else if (rtttl::done())
  {
    if(lastIsPlaying){
      melodyNode.setProperty("playing").send("false");
      lastIsPlaying = false;
    }
  }
  
  button.tick();
  Homie.loop();
}